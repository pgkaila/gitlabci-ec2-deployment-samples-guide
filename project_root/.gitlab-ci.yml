# Validate gitlab-ci.yml: https://gitlab.com/ci/lint
# Gitlab-CI Sample example
# Uses Gradle and Java 8 VM

# Sets image file for Container
image: java:8

# Script executed before the various pipeline build stages are run - sets up the environment
before_script:
  - export GRADLE_USER_HOME=`pwd`/.gradle
  - apt-get update --quiet --yes
  - apt-get --quiet install --yes wget unzip
  - wget --quiet --output-document=gradle.zip https://services.gradle.org/distributions/gradle-2.14.1-bin.zip
  - unzip -u -q gradle.zip

# Defines the stages in the Gitlab-CI Pipeline
stages:
  - build
  - cleanup_build
  - test
  - deploy

# Defines the Container environment variables
variables:
  G: gradle-2.14.1/bin/gradle         # defines path for Gradle
  BUNDLE_PATH: build/distributions    # defines the path for the generated ZIP file
  BUNDLE_SRC: $BUNDLE_PATH/my_app.zip # defines the zipfile to upload to S3


### BUILD ####################################################
build-job:
  # Script to run for building the application
  script:
    - $G build -x test -x distZip -x distTar # shell command to build application with gradle
  # Current stage in the pipeline
  stage: build
  # Cache information
  cache:
    key: "$CI_BUILD_NAME/$CI_BUILD_REF_NAME"
    untracked: true
    paths:
        - build/
  # Applies only to branches
  only:
    - branches
  # Excludes commits for tags and triggers
  except:
    - triggers

build-clean-job:
  script:
    - $G clean
  when: on_failure

### TEST ####################################################
test-job:
  # Script to run for building and testing the application
  script:
    - $G test -x distZip -x distTar
  stage: test
  cache:
    key: "$CI_BUILD_NAME/$CI_BUILD_REF_NAME"
    untracked: true
    paths:
        - build/
  only:
    - branches
    - /^v\d+\.\d+\.\d+-.*$/
  except:
    - triggers

### DEPLOY ####################################################
deploy-job:
  # Script to run for deploying application to AWS
  script:
    - apt-get --quiet install --yes python-pip # AWS CLI requires python-pip, python is installed by default
    - pip install -U pip  # pip update
    - pip install awscli  # AWS CLI installation
    - $G build -x test -x distTar # # Build the project with Gradle
    - $G distZip  # creates distribution zip for deployment
    - aws s3 cp $BUNDLE_SRC $AWS_S3_LOCATION # Uploads the zipfile to S3 and expects the AWS Code Pipeline/Code Deploy to pick up
  # requires previous CI stages to succeed in order to execute
  when: on_success
  stage: deploy #assigns the stage as deploy
  environment: production # Assign the Gitlab Environment for tracking purposes
  cache:
    key: "$CI_BUILD_NAME/$CI_BUILD_REF_NAME"
    untracked: true
    paths:
        - build/
  # Applies only to tags matching the regex: ie: v1.0.0-My-App-Release
  only:
    - /^v\d+\.\d+\.\d+-.*$/
  except:
    - branches
    - triggers

##############################################################
# Script executed following each pipeline stage script
after_script:
   - mkdir -p /cache/.gradle  # creates a directory in the container global cache
   - cp -Ru .gradle/caches /cache/.gradle # copies the gradle caches directory to the global cache